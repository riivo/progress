TRIGGER PROCEDURE FOR CREATE OF customer.
customerNum = NEXT-VALUE (customerNum).

CREATE invoice.
invoice.invoiceNum = NEXT-VALUE (invoiceNum).
invoice.customerNum = customer.customerNum.
FIND item WHERE item.itemNum = 1 NO-LOCK.
invoice.total = item.price.
invoice.created = NOW.

CREATE db.invoiceRow.
db.invoiceRow.invoiceRowNum = NEXT-VALUE (invoiceRowNum).
db.invoiceRow.invoiceNum = db.invoice.invoiceNum.
db.invoiceRow.itemNum = 1.
db.invoiceRow.price = db.item.price.
db.invoiceRow.name = db.item.name.
