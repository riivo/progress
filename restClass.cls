@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
USING Progress.Lang.*.
BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS restClass: 

    DEFINE TEMP-TABLE ttCustomer NO-UNDO LIKE db.customer
        FIELD errmsg AS CHARACTER.
    DEFINE TEMP-TABLE ttItem NO-UNDO LIKE db.item 
        FIELD errmsg AS CHARACTER.
    DEFINE TEMP-TABLE ttInvoice NO-UNDO LIKE db.invoice 
        FIELD errmsg AS CHARACTER.
    DEFINE TEMP-TABLE ttInvoiceRow NO-UNDO LIKE db.invoiceRow.
    DEFINE DATASET dsInvoice FOR ttInvoice, ttInvoiceRow, ttItem
        DATA-RELATION r1 FOR ttInvoice, ttInvoiceRow RELATION-FIELDS(invoiceNum, invoiceNum) NESTED
        DATA-RELATION r2 FOR ttInvoiceRow, ttItem RELATION-FIELDS(itemNum, itemNum) NESTED.

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getCustomer(INPUT customerNum AS INTEGER, OUTPUT TABLE ttCustomer):
        
        EMPTY TEMP-TABLE ttCustomer.
        IF customerNum = 0 THEN 
        DO:
            FOR EACH db.customer NO-LOCK:
                CREATE ttCustomer.
                BUFFER-COPY customer TO ttCustomer.
            END.
            RETURN.
        END.
        IF CAN-FIND (FIRST db.customer WHERE db.customer.customerNum = customerNum NO-LOCK) THEN 
        DO:
            FIND FIRST customer WHERE customer.customerNum = customerNum NO-LOCK.
            CREATE ttCustomer.
            BUFFER-COPY customer TO ttCustomer.
        END.
        ELSE
        DO:
            CREATE ttCustomer.
            ttCustomer.errmsg = "Sellist klienti pole. ID: " + STRING (customerNum).
        END.
        RETURN.

    END METHOD.

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getItem(INPUT itemNum AS INTEGER, OUTPUT TABLE ttItem):
        
        EMPTY TEMP-TABLE ttItem.
        IF itemNum = 0 THEN 
        DO:
            FOR EACH db.item NO-LOCK:
                CREATE ttItem.
                BUFFER-COPY db.item TO ttItem.
            END.
            RETURN.
        END.
        IF CAN-FIND (FIRST db.item WHERE item.itemNum = itemNum NO-LOCK) THEN
        DO:
            FIND FIRST db.item WHERE item.itemNum = itemNum NO-LOCK.
            CREATE ttItem.
            BUFFER-COPY db.item TO ttItem.
        END.
        ELSE
        DO:
            CREATE ttItem.
            ttItem.errmsg = "Sellist toodet pole. ID: " + STRING (itemNum).
        END.
        RETURN.

    END METHOD.
    
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID createCustomer (INPUT name AS CHARACTER, INPUT address AS CHARACTER, OUTPUT TABLE ttCustomer):
        
        EMPTY TEMP-TABLE ttCustomer.
        
        IF name = ? OR address = ? THEN
        DO:
            CREATE ttCustomer.
            ttCustomer.errmsg = "Nimi ja aadress peavad olema määratud".
            RETURN.
        END.

        IF CAN-FIND (FIRST db.customer WHERE db.customer.name = name NO-LOCK) THEN
        DO:
            CREATE ttCustomer.
            FIND FIRST db.customer WHERE db.customer.name = NAME NO-LOCK.
            ttCustomer.errmsg = "Selline klient on juba olemas. ID: " + STRING (db.customer.customerNum).
            RETURN.
        END.
        
        CREATE db.customer.
        ASSIGN customer.name = name
            customer.address = address
            db.customer.created = NOW.
        BUFFER-COPY customer TO ttCustomer.
        
        RETURN.

    END METHOD.
    
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID createItem (INPUT name AS CHARACTER, INPUT price AS DECIMAL, INPUT category AS CHARACTER, OUTPUT TABLE ttItem):
        
        EMPTY TEMP-TABLE ttItem.
        
        IF name = ? OR price = ? OR category = ? THEN 
        DO:
            CREATE ttItem.
            ttItem.errmsg = "Nimi, hind ja kategooria peavad olema määratud".
            RETURN.
        END.

        IF CAN-FIND (FIRST db.item WHERE db.item.name = name NO-LOCK) THEN 
        DO:
            CREATE ttItem.
            FIND FIRST db.item WHERE db.item.name = name NO-LOCK.
            ttItem.errmsg = "Selline toode on juba olemas. ID: " + STRING (db.item.itemNum).
            RETURN.
        END.
        
        CREATE db.item.
        ASSIGN db.item.itemNum = NEXT-VALUE (itemNum)
            db.item.name = name
            db.item.category = category
            db.item.price = price
            db.item.created = NOW.
        BUFFER-COPY item TO ttItem.

        RETURN.

    END METHOD.  
      
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID createInvoice (INPUT customerNum AS INTEGER, INPUT item AS CHARACTER, OUTPUT DATASET dsInvoice):
        
        DEFINE VARIABLE i AS INTEGER.
        DEFINE VARIABLE total AS INTEGER.
        DEFINE VARIABLE err AS CHARACTER INITIAL "".
        
        EMPTY TEMP-TABLE ttInvoice.
        EMPTY TEMP-TABLE ttInvoiceRow.
        EMPTY TEMP-TABLE ttItem.
        
        IF CAN-FIND (FIRST db.customer WHERE db.customer.customerNum = customerNum NO-LOCK) THEN
        DO:
            CREATE db.invoice.
            ASSIGN db.invoice.invoiceNum = NEXT-VALUE (invoiceNum)
                db.invoice.customerNum = customerNum
                db.invoice.created = NOW.
    
            REPEAT i = 1 TO NUM-ENTRIES (item).
                IF CAN-FIND (item WHERE item.itemNum = INTEGER (ENTRY (i, item)) NO-LOCK) THEN
                DO:
                    CREATE db.invoiceRow.
                    ASSIGN db.invoiceRow.invoiceRowNum = NEXT-VALUE (invoiceRowNum)
                        db.invoiceRow.invoiceNum = db.invoice.invoiceNum
                        db.invoiceRow.itemNum = INTEGER (ENTRY (i, item)).
                    FIND item WHERE item.itemNum = INTEGER (ENTRY (i, item)) NO-LOCK.
                    db.invoiceRow.price = db.item.price.
                    db.invoiceRow.name = db.item.name.
                    ACCUMULATE db.item.price (TOTAL).
                    CREATE ttInvoiceRow.
                    BUFFER-COPY db.invoiceRow TO ttInvoiceRow.
                END.
                ELSE
                DO:
                    err = err + "Puudub toode. ID: " + ENTRY (i, item) + " ".
                END.
            END.
            
            db.invoice.total = ACCUMULATE TOTAL (db.item.price).
            CREATE ttInvoice.
            ttInvoice.errmsg = err.
            BUFFER-COPY invoice TO ttInvoice.
        END.
        ELSE
        DO:
            CREATE ttInvoice.
            ttInvoice.errmsg = "Sellist klienti pole. ID: " + STRING (customerNum).
        END.
        RETURN.

    END METHOD. 
    
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getItemByCategory (INPUT category AS CHARACTER, OUTPUT TABLE ttItem):
        
        EMPTY TEMP-TABLE ttItem.
        FOR EACH db.item WHERE db.item.category = category NO-LOCK:
            CREATE ttItem.
            BUFFER-COPY item TO ttItem.
        END.

        RETURN.

    END METHOD.

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getInvoiceByCustomer (INPUT customerNum AS INTEGER, OUTPUT DATASET dsInvoice):
        
        EMPTY TEMP-TABLE ttInvoice.
        EMPTY TEMP-TABLE ttInvoiceRow.
        EMPTY TEMP-TABLE ttItem.
        FOR EACH db.invoice WHERE db.invoice.customerNum = customerNum NO-LOCK:
            CREATE ttInvoice.
            BUFFER-COPY db.invoice TO ttInvoice.
            FOR EACH db.invoiceRow WHERE db.invoiceRow.invoiceNum = db.invoice.invoiceNum NO-LOCK:
                CREATE ttInvoiceRow.
                BUFFER-COPY db.invoiceRow TO ttInvoiceRow.
                FOR EACH db.item WHERE db.item.itemNum = db.invoiceRow.itemNum NO-LOCK:
                    IF NOT CAN-FIND (ttItem WHERE ttItem.itemNum = db.item.itemNum NO-LOCK) THEN
                    DO:
                        CREATE ttItem.
                        BUFFER-COPY db.item TO ttItem.
                    END.
                END.
            END.
        END.

        RETURN.

    END METHOD.
    
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID toggleItem (INPUT itemNum AS INTEGER, INPUT active AS LOGICAL, OUTPUT TABLE ttItem):
        
        EMPTY TEMP-TABLE ttItem.
        IF CAN-FIND (FIRST db.item WHERE item.itemNum = itemNum NO-LOCK) THEN
        DO:
            FIND FIRST db.item WHERE item.itemNum = itemNum EXCLUSIVE-LOCK.
            IF active = YES THEN 
            DO:
                item.active = YES.
            END.
            ELSE 
            DO:
                item.active = NO.
            END.
            CREATE ttItem.
            BUFFER-COPY item TO ttItem.
        END.
        ELSE
        DO:
            CREATE ttItem.
            ttItem.errmsg = "Sellist toodet pole. ID: " + STRING (itemNum).
        END.
        RETURN.

    END METHOD.

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID editItem (INPUT itemNum AS INTEGER, INPUT name AS CHARACTER, INPUT price AS DECIMAL, INPUT category AS CHARACTER, OUTPUT TABLE ttItem):
        
        EMPTY TEMP-TABLE ttItem.
        IF name = ? OR price = ? OR category = ? THEN 
        DO:
            CREATE ttItem.
            ttItem.errmsg = "Nimi, hind ja kategooria peavad olema määratud".
            RETURN.
        END.

        IF CAN-FIND (FIRST db.item WHERE db.item.name = name NO-LOCK) THEN 
        DO:
            FIND FIRST db.item WHERE db.item.name = name EXCLUSIVE-LOCK.
            IF db.item.itemNum = itemNum THEN
            DO:
                db.item.price = price.
                db.item.category = category.
                CREATE ttItem.
                BUFFER-COPY item TO ttItem.
            END.
            ELSE
            DO:
                CREATE ttItem.
                ttItem.errmsg = "Selline toode on juba olemas. ID: " + STRING (db.item.itemNum).
            END.
            RETURN.
        END.
        
        IF CAN-FIND (FIRST db.item WHERE db.item.itemNum = itemNum NO-LOCK) THEN
        DO:
            FIND FIRST db.item WHERE item.itemNum = itemNum EXCLUSIVE-LOCK.
            ASSIGN db.item.name = name
                db.item.price = price
                db.item.category = category.
            CREATE ttItem.
            BUFFER-COPY item TO ttItem.
        END.
        ELSE
        DO:
            CREATE ttItem.
            ttItem.errmsg = "Toodet pole. ID: " + STRING (itemNum).
        END.
        RETURN.

    END METHOD.
    
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID editCustomer (INPUT customerNum AS INTEGER, INPUT name AS CHARACTER, INPUT address AS CHARACTER, OUTPUT TABLE ttCustomer):
        
        EMPTY TEMP-TABLE ttCustomer.
        IF name = ? OR address = ? THEN
        DO:
            CREATE ttCustomer.
            ttCustomer.errmsg = "Nimi ja aadress peavad olema määratud".
            RETURN.
        END.

        IF CAN-FIND (FIRST db.customer WHERE db.customer.name = name NO-LOCK) THEN
        DO:
            FIND FIRST db.customer WHERE db.customer.name = name EXCLUSIVE-LOCK.
            IF db.customer.customerNum = customerNum THEN
            DO:
                db.customer.address = address.
                CREATE ttCustomer.
                BUFFER-COPY customer TO ttCustomer.
                RETURN.
            END.
            ELSE
            DO:
                CREATE ttCustomer.
                ttCustomer.errmsg = "Selline klient on juba olemas. ID: " + STRING (db.customer.customerNum).
            END.
            RETURN.
        END.
        
        IF CAN-FIND (FIRST db.customer WHERE db.customer.customerNum = customerNum NO-LOCK) THEN
        DO:
            FIND FIRST db.customer WHERE db.customer.customerNum = customerNum EXCLUSIVE-LOCK.
            db.customer.name = name.
            db.customer.address = address.
            CREATE ttCustomer.
            BUFFER-COPY customer TO ttCustomer.
        END.
        ELSE
        DO:
            CREATE ttCustomer.
            ttCustomer.errmsg = "Klienti pole. ID: " + STRING (customerNum).
        END.
        
        RETURN.

    END METHOD.
    
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getInvoice (INPUT invoiceNum AS INTEGER, OUTPUT TABLE ttInvoiceRow):
        
        EMPTY TEMP-TABLE ttInvoiceRow.
        FOR EACH db.invoiceRow WHERE db.invoiceRow.invoiceNum = invoiceNum NO-LOCK:
            CREATE ttInvoiceRow.
            BUFFER-COPY invoiceRow TO ttInvoiceRow.
        END.
        RETURN.

    END METHOD.
    
END CLASS.